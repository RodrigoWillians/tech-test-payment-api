using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.DTO
{
    public class VendedorDTO
    {
        [Required(ErrorMessage ="O CPF é obrigatório", AllowEmptyStrings =false)]
        [StringLength(11, MinimumLength =11)]
        public string CPF { get; set; }

        [Required(ErrorMessage ="O nome do vendedor é obrigatório", AllowEmptyStrings =false)]
        [StringLength(50, MinimumLength =4)]
        public string Nome { get; set; }

        [Required]
        [RegularExpression(".+\\@.+\\..+",ErrorMessage ="Informe um email valido.")]
        public string Email { get; set; }

        [Required]
        [StringLength(15, MinimumLength =11)]
        public string Telefone { get; set; }

        [Required]
        [DisplayFormat(DataFormatString ="dd/mm/yyyy")]
        public DateTime DataNascimento { get; set; }

        [Required]
        public bool Status { get; set; }
    }
}