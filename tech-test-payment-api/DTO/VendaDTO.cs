using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using tech_test_payment_api.Model;

namespace tech_test_payment_api.DTO
{
    public class VendaDTO
    {
        [Required(ErrorMessage ="O id do vendedor é obrigatório", AllowEmptyStrings = false)]
        public int IdVendedor { get; set; }
        [Required(ErrorMessage ="O id do produto é obrigatório", AllowEmptyStrings = false)]
        public List<ItemVendaDTO> ItemVendas {get; set; }

    }
}