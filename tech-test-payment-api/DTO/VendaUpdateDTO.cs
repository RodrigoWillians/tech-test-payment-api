using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.DTO
{
    public class VendaUpdateDTO
    {
        [Required(ErrorMessage ="O status da venda é obrigatório", AllowEmptyStrings = false)]
        public string StatusVenda { get; set; }
        public VendaUpdateDTO(int i,int j,string status, decimal valor) 
        {
            int _i = i;
            int _j = j;
            string _status = status;
            decimal _valor = valor;
        }

    }
}