using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.DTO
{
    public class ProdutoUpdateDTO
    {
        [Required(ErrorMessage ="O nome do produto é obrigatório", AllowEmptyStrings =false)]
        [StringLength(50, MinimumLength =4)]
        public string Nome { get; set; }

        [Required]
        public decimal Valor { get; set; }
    }
}