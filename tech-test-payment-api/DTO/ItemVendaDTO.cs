using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.DTO
{
    public class ItemVendaDTO
    {
        public int IdProduto { get; set; }
        public int Quantidade{get; set; }
    }
}