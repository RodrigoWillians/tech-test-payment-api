using System.ComponentModel.DataAnnotations;


namespace tech_test_payment_api.Model
{
    public class Venda
    {
        [Key]
        public int Id { get; set; }
        public Vendedor? IdVendedor { get; set; }
        public decimal Total { get; set; }
        public string StatusVenda { get; set; }
    }
}