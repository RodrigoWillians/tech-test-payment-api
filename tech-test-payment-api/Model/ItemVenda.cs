using System.ComponentModel.DataAnnotations;


namespace tech_test_payment_api.Model
{
    public class ItemVenda
    {
        [Key]
        public int OrdemPedido { get; set; }
        [Key]
        public Venda IdVenda { get; set; }
        public Produto IdProduto { get; set; }
        public int Quantidade { get; set; }
        public decimal Total { get; set; }

    }
}