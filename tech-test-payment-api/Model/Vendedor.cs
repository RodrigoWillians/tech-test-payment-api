
namespace tech_test_payment_api.Model
{
    public class Vendedor
    {
        
        public int Id { get; set; }
        public string CPF { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
        public DateTime DataNascimento { get; set; }
        public bool Status { get; set; }
    }
}