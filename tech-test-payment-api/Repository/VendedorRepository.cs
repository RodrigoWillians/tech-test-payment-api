using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.DatabaseContext;
using tech_test_payment_api.Model;
using tech_test_payment_api.Repository.Interfaces;

namespace tech_test_payment_api.Repository
{
    public class VendedorRepository: BaseRepository, IVendedorRepository
    {
        private readonly PaymentContext _paymentContext;

        public VendedorRepository(PaymentContext paymentContext) : base (paymentContext){
            _paymentContext = paymentContext;
        }
        public async Task <IEnumerable<Vendedor>> BuscarTodosVendedores(){
            var vendedores = await _paymentContext.Vendedores.Where(v => v.Status == true).AsNoTracking().ToListAsync();
            return vendedores;
        }

        public async Task <Vendedor> BuscarVendedor(int id, bool pesquisa){
            Vendedor consulta;

            if (pesquisa)
            {
                consulta = await _paymentContext.Vendedores
                                                    .Where(v => v.Id == id && v.Status == true)
                                                    .AsNoTracking()
                                                    .FirstOrDefaultAsync();
            }
            else
            {
                consulta = await _paymentContext.Vendedores
                                                    .Where(v => v.Id == id && v.Status == true)
                                                    .FirstOrDefaultAsync();
            }

            return (Vendedor)consulta;
        }

    }
}