using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.DatabaseContext;
using tech_test_payment_api.Model;
using tech_test_payment_api.Repository.Interfaces;

namespace tech_test_payment_api.Repository
{
    public class ProdutoRepository : BaseRepository, IProdutoRepository
    {
        private readonly PaymentContext _paymentContext;

        public ProdutoRepository(PaymentContext paymentContext) : base (paymentContext){
            _paymentContext = paymentContext;
        }
        public async Task <IEnumerable<Produto>> BuscarTodosProdutos(){
            var produtos = await _paymentContext.Produtos.Where(v => v.Status == true).AsNoTracking().ToListAsync();
            return produtos;
        }

        public async Task <Produto> BuscarProduto(int id, bool pesquisa){
            Produto consulta;

            if (pesquisa)
            {
                consulta = await _paymentContext.Produtos
                                                    .Where(v => v.Id == id && v.Status == true)
                                                    .AsNoTracking()
                                                    .FirstOrDefaultAsync();
            }
            else
            {
                consulta = await _paymentContext.Produtos
                                                    .Where(v => v.Id == id && v.Status == true)
                                                    .FirstOrDefaultAsync();
            }

            return (Produto)consulta;
        }
    }
}