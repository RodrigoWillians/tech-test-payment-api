using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Repository.Interfaces
{
    public interface IBaseRepository
    {
        public void Add<T>(T entidade) where T : class;

        public void Update<T>(T entidade) where T : class;

        Task<bool> SalvarDados();

        public void AddRange<T>(List<T> entidade) where T : class;
    }
}