using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Model;

namespace tech_test_payment_api.Repository.Interfaces
{
    public interface IVendaRepository : IBaseRepository
    {
        public Task<Vendedor> GetVendedorAsync(int id);
        public Task<Produto> GetProdutoAsync(int id);
        public Task<Venda> GetVendaLastAsync();
        public Task<Venda> GetVendaIDAsync(int id);
    }
}