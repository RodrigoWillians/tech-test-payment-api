using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Model;

namespace tech_test_payment_api.Repository.Interfaces
{
    public interface IVendedorRepository : IBaseRepository
    {
        public Task<IEnumerable<Vendedor>> BuscarTodosVendedores();
        public Task<Vendedor> BuscarVendedor(int id, bool pesquisa);
    }
}