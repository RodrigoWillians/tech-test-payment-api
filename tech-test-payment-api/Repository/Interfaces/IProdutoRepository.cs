using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Model;

namespace tech_test_payment_api.Repository.Interfaces
{
    public interface IProdutoRepository : IBaseRepository
    {
        public Task<IEnumerable<Produto>> BuscarTodosProdutos();
        public Task<Produto> BuscarProduto(int id, bool pesquisa);
    }
}