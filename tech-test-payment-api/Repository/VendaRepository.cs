using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.DatabaseContext;
using tech_test_payment_api.Model;
using tech_test_payment_api.Repository.Interfaces;

namespace tech_test_payment_api.Repository
{
    public class VendaRepository : BaseRepository , IVendaRepository
    {
        private readonly PaymentContext _paymentContext;
        public VendaRepository(PaymentContext paymentContext) : base(paymentContext)
        {
            _paymentContext = paymentContext;
        }
        public async Task<Vendedor> GetVendedorAsync(int id) {
            Vendedor? vendedor = await _paymentContext.Vendedores.Where(v =>
                                     v.Id == id && v.Status  == true)
                                    .FirstOrDefaultAsync();
            return vendedor;
        }
        public async Task<Produto> GetProdutoAsync(int id) {
            Produto? produto = await _paymentContext.Produtos.Where(p =>
                                     p.Id == id && p.Status  == true)
                                    .FirstOrDefaultAsync();
            return produto;
        }
        public async Task<Venda> GetVendaLastAsync() {
             Venda? venda = await _paymentContext.Vendas  
                                .OrderBy(ve => ve.Id)                                                        
                                .LastAsync();                    
            return venda;
        }

        public async Task<Venda> GetVendaIDAsync(int id) {
            Venda? venda = await _paymentContext.Vendas.Where(ve => ve.Id == id).FirstOrDefaultAsync();
            return venda;
        }
    }

}