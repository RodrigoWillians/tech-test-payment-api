using tech_test_payment_api.DatabaseContext;
using tech_test_payment_api.Repository.Interfaces;

namespace tech_test_payment_api.Repository
{
    public class BaseRepository : IBaseRepository
    {
        private readonly PaymentContext _paymentContext;
        public BaseRepository(PaymentContext paymentContext)
        {
            _paymentContext = paymentContext;
        }

        public void Add<T>(T entidade) where T : class {
            _paymentContext.Add(entidade);
        }

        public void AddRange<T>(List<T> entidade) where T : class {
            _paymentContext.AddRange(entidade);
        }

        public void Update<T>(T entidade) where T : class {
            _paymentContext.Update(entidade);
        }

        public async Task<bool> SalvarDados() {
            return await _paymentContext.SaveChangesAsync() > 0;
        }
    }
}