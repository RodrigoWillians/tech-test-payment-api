using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Model;

namespace tech_test_payment_api.DatabaseContext
{
    public class PaymentContext : DbContext
    {
        public PaymentContext(DbContextOptions<PaymentContext> options) : base(options) { }

        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Venda> Vendas { get; set; }
        public DbSet<ItemVenda> itemVendas { get; set; }
    }
}