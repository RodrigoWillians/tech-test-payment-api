using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Model;
using tech_test_payment_api.Repository;
using tech_test_payment_api.Repository.Interfaces;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ProdutoController : ControllerBase
    {
                private readonly IProdutoRepository _produtoRepository;

        public ProdutoController(IProdutoRepository produtoRepository)
        {
            _produtoRepository = produtoRepository;
        }

        [HttpGet]
        public async Task<IActionResult> BuscarTodosProdutos()
        {
            try
            {
                var produtos = await _produtoRepository.BuscarTodosProdutos();
                if (produtos.Any() == false)
                    return StatusCode(404, "Não foi encontrado nenhum produto");
                else
                    return StatusCode(200, produtos);


            }
            catch (Exception)
            {

                return StatusCode(500, "Erro de processamento interno");
            }
        }


        [HttpGet("(id)")]

        public async Task<IActionResult> BuscarProduto(int id)
        {
            try
            {
                var produto = await _produtoRepository.BuscarProduto(id, true);
                if (produto == null)
                    return StatusCode(404, "Não foi encontrado nenhum produto");
                else
                    return StatusCode(200, produto);
            }
            catch (Exception)
            {
                return StatusCode(500, "Erro de processamento interno");
            }
        }


        [HttpPost]
        public async Task<IActionResult> CreateProduto(ProdutoDTO produto)
        {
            try
            {
                Produto produtoModel = new();
                produtoModel.Nome = produto.Nome;
                produtoModel.Valor = produto.Valor;
                produtoModel.Status = true;

                _produtoRepository.Add(produtoModel);
                await _produtoRepository.SalvarDados();
                return StatusCode(201, produtoModel);

            }
            catch (Exception)
            {
                return StatusCode(500, "Ocorreu um erro ao gravar o produto.");
            }
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> AtualizarProduto(ProdutoUpdateDTO produto, int id)
        {
            try
            {
                var buscaProduto = await _produtoRepository.BuscarProduto(id, false);
                if (buscaProduto == null)
                    return StatusCode(404, "Não foi encontrado nenhum produto");

                buscaProduto.Nome = produto.Nome;
                buscaProduto.Valor = produto.Valor;


                _produtoRepository.Update(buscaProduto);
                await _produtoRepository.SalvarDados();
                return StatusCode(200, "O produto foi atualizado com sucesso!");

            }
            catch (Exception)
            {

                return StatusCode(500, "Ocorreu um erro ao gravar o produto.");
            }
        }

        [HttpDelete]
        public async Task<IActionResult> DeletarProduto(int id)
        {
            try
            {
                var buscaProduto = await _produtoRepository.BuscarProduto(id, false);
                if (buscaProduto == null)
                    return StatusCode(404, "Não foi encontrado nenhum produto");

                buscaProduto.Status = false;

                _produtoRepository.Update(buscaProduto);
                await _produtoRepository.SalvarDados();
                return StatusCode(200, "O produto foi deletado com sucesso!");

            }
            catch (Exception)
            {

                return StatusCode(500, "Ocorreu um erro ao deletar o produto.");
            }
        }
    }
}