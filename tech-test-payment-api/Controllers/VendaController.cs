using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Model;
using tech_test_payment_api.Repository.Interfaces;

namespace apipagamento_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly IVendaRepository _vendaRepository;

        public VendaController(IVendaRepository vendaRepository)
        {
            _vendaRepository = vendaRepository;
        }
        [HttpPost]
        public async Task<IActionResult> CreateVenda(VendaDTO venda)
        {
            try {
                Vendedor vendedorId = await _vendaRepository.GetVendedorAsync(venda.IdVendedor);
                if (vendedorId == null) 
                   return StatusCode(400,"Vendedor não encontrado e não possivel realizar a venda");
                ItemVenda itemVendaModel = new();
                foreach (var item in venda.ItemVendas)
                {
                    Produto produtoId = await _vendaRepository.GetProdutoAsync(Convert.ToInt32( item.IdProduto));
                    if (produtoId == null) 
                        return StatusCode(400,"Deve existir pelo menos 1 item do produto");
                    itemVendaModel.Total += item.Quantidade * produtoId.Valor;
                }                
                
                Venda vendaModel = new()
                {
                    IdVendedor = vendedorId,
                    Total = itemVendaModel.Total,
                    StatusVenda = "Aguardando Pagamento"
                };
                _vendaRepository.Add(vendaModel);
                await _vendaRepository.SalvarDados();

                List<ItemVenda> itemVendaListModel = new List<ItemVenda>();

                foreach (var item in venda.ItemVendas)
                {
                    ItemVenda itemVendaModelList = new ItemVenda();
                    var IdProduto = itemVendaModelList.IdProduto = await _vendaRepository.GetProdutoAsync(Convert.ToInt32( item.IdProduto));
                    var vendaId = await _vendaRepository.GetVendaLastAsync();
                    itemVendaModelList.IdVenda =  vendaId;

                    itemVendaModelList.Quantidade = item.Quantidade;
                    _vendaRepository.Add(itemVendaModelList);
                    itemVendaModelList.Total = item.Quantidade * IdProduto.Valor;
                }

                _vendaRepository.AddRange(itemVendaListModel);
                await _vendaRepository.SalvarDados();



                return StatusCode(201, "Venda criada com sucesso");               
            }
            catch (Exception)
            {
                return StatusCode(500, "Houve erro ao gravar a venda");
            }
        }

[HttpPut]
        public async Task<IActionResult> UpdateStatusVenda(VendaUpdateDTO vendaStatus, int id) 
        {
            try {
            var IdVenda =await  _vendaRepository.GetVendaIDAsync( id);

            if (IdVenda == null) 
                return StatusCode(400,"Venda não encontrada");
            if
            (
                IdVenda.StatusVenda.Equals("Aguardando Pagamento") && vendaStatus.StatusVenda.Equals("Cancelado") 
            ||    
                IdVenda.StatusVenda.Equals("Aguardando Pagamento") && vendaStatus.StatusVenda.Equals("Pagamento Aprovado")
            )
            {
                IdVenda.StatusVenda = vendaStatus.StatusVenda;
            }
            else if
            (
                IdVenda.StatusVenda.Equals("Pagamento Aprovado") && vendaStatus.StatusVenda.Equals("Cancelado") 
            ||    
                IdVenda.StatusVenda.Equals("Pagamento Aprovado") && vendaStatus.StatusVenda.Equals("Enviado Transportadora")
            )
            {
                IdVenda.StatusVenda = vendaStatus.StatusVenda;
            }
            else if 
            (
                IdVenda.StatusVenda.Equals("Enviado Transportadora") && vendaStatus.StatusVenda.Equals("Entregue") 
            )
            {
                IdVenda.StatusVenda = vendaStatus.StatusVenda;
            }
            else
            {
                return StatusCode(400,"Status da venda não foi enviado corretamente");
            }
            _vendaRepository.Update(IdVenda);
            await _vendaRepository.SalvarDados();
            return StatusCode(201, "Status da venda atualizada com sucesso");
            }
            catch (Exception) {
                return StatusCode(500, "Houve erro ao atualizar o status da venda");
            }
        }
    }
}