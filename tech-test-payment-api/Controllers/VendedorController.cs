using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Model;
using tech_test_payment_api.Repository.Interfaces;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly IVendedorRepository _vendedorRepository;

        public VendedorController(IVendedorRepository vendedorRepository)
        {
            _vendedorRepository = vendedorRepository;
        }

        [HttpGet]
        public async Task<IActionResult> BuscarTodosVendedores()
        {
            try
            {
                var vendedores = await _vendedorRepository.BuscarTodosVendedores();
                if (vendedores.Any() == false)
                    return StatusCode(404, "Não foi encontrado nenhum vendedor");
                else
                    return StatusCode(200, vendedores);


            }
            catch (Exception)
            {

                return StatusCode(500, "Erro de processamento interno");
            }
        }


        [HttpGet("(id)")]

        public async Task<IActionResult> BuscarVendedor(int id)
        {
            try
            {
                var vendedor = await _vendedorRepository.BuscarVendedor(id, true);
                if (vendedor == null)
                    return StatusCode(404, "Não foi encontrado nenhum vendedor");
                else
                    return StatusCode(200, vendedor);
            }
            catch (Exception)
            {
                return StatusCode(500, "Erro de processamento interno");
            }
        }


        [HttpPost]
        public async Task<IActionResult> CreateVendedor(VendedorDTO vendedor)
        {
            try
            {
                Vendedor vendedorModel = new();
                vendedorModel.CPF = vendedor.CPF;
                vendedorModel.Nome = vendedor.Nome;
                vendedorModel.Email = vendedor.Email;
                vendedorModel.Telefone = vendedor.Telefone;
                vendedorModel.DataNascimento = vendedor.DataNascimento;
                vendedorModel.Status = true;
                _vendedorRepository.Add(vendedorModel);
                await _vendedorRepository.SalvarDados();
                return StatusCode(201, vendedorModel);

            }
            catch (Exception)
            {
                return StatusCode(500, "Ocorreu um erro ao gravar o vendedor.");
            }
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> AtualizarVendedor(VendedorUpdateDTO vendedor, int id)
        {
            try
            {
                var buscaVendedor = await _vendedorRepository.BuscarVendedor(id, false);
                if (buscaVendedor == null)
                    return StatusCode(404, "Não foi encontrado nenhum vendedor");

                buscaVendedor.CPF = vendedor.CPF;
                buscaVendedor.Nome = vendedor.Nome;
                buscaVendedor.Email = vendedor.Email;
                buscaVendedor.Telefone = vendedor.Telefone;
                buscaVendedor.DataNascimento = vendedor.DataNascimento;

                _vendedorRepository.Update(buscaVendedor);
                await _vendedorRepository.SalvarDados();
                return StatusCode(200, "O vendedor foi atualizado com sucesso!");

            }
            catch (Exception)
            {

                return StatusCode(500, "Ocorreu um erro ao gravar o vendedor.");
            }
        }

        [HttpDelete]
        public async Task<IActionResult> DeletarVendedor(int id)
        {
            try
            {
                var buscaVendedor = await _vendedorRepository.BuscarVendedor(id, false);
                if (buscaVendedor == null)
                    return StatusCode(404, "Não foi encontrado nenhum vendedor");

                buscaVendedor.Status = false;

                _vendedorRepository.Update(buscaVendedor);
                await _vendedorRepository.SalvarDados();
                return StatusCode(200, "O vendedor foi deletado com sucesso!");

            }
            catch (Exception)
            {

                return StatusCode(500, "Ocorreu um erro ao deletar o vendedor.");
            }
        }
    }
}