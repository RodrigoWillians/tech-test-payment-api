using tech_test_payment_api.DatabaseContext;
using tech_test_payment_api.Model;

namespace tech_test_payment_api_test
{
    public class DbUnitTestMockInitializer
    {
        public DbUnitTestMockInitializer() { }

        public void Seed(PaymentContext context)
        {
            //var vendedor = new VendedorControllerTest();

            context.Vendedores.Add(new Vendedor
            {
                Id = 1,
                CPF = "36263406860",
                Nome = "thiago",
                Email = "rodrigo@gmail.com",
                Telefone = "13997982603",
                DataNascimento = DateTime.Now,
                Status = true

            });
            context.Vendedores.Add(new Vendedor
            {
                Id = 2,
                CPF = "11223312236",
                Nome = "rodrigo",
                Email = "dsfsdfds@gmail.com",
                Telefone = "13665588987",
                DataNascimento = DateTime.Now,
                Status = true

            });


            context.Produtos.Add(new Produto
            {
                Id = 1,
                Nome = "feijao",
                Valor = 10.00M,
                Status = true

            });

            context.Produtos.Add(new Produto
            {
                Id = 2,
                Nome = "arroz",
                Valor = 5.00M,
                Status = true

            });

            context.Vendas.Add(new Venda
            {
                Id = 1,
                IdVendedor = context.Vendedores.Where(ven => ven.Id == 1).FirstOrDefault(),
                StatusVenda = "Aguardando Pagamento",
                Total = 10.00M
            });

            context.SaveChanges();
        }

    }
}