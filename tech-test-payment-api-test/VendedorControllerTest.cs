using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Controllers;
using tech_test_payment_api.DatabaseContext;
using tech_test_payment_api.Model;
using tech_test_payment_api.Repository;
using tech_test_payment_api.Repository.Interfaces;

namespace tech_test_payment_api_test
{
    public class VendedorControllerTest
    {

        private static IVendedorRepository _vendedorRepository;


        static VendedorControllerTest()
        {
            var dbOptions = new DbContextOptionsBuilder<PaymentContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;
            var context = new PaymentContext(dbOptions);
            var db = new DbUnitTestMockInitializer();
            db.Seed(context);
            _vendedorRepository = new VendedorRepository(context);
        }

        [Fact]
        public async Task GetVendedorId_DeveRetornarSomenteUmVendedor() {
            var expectedid = 1;
            var vendedorController = new VendedorController(_vendedorRepository);
            var result = await _vendedorRepository.BuscarVendedor(expectedid, true);
            Assert.Equal(expectedid, result.Id);
        }

        [Fact]
        public async Task GetTodosVendedores_DeveRetornarSomenteUmVendedor() {
            List<Vendedor> vendedor = new List<Vendedor>();

            var numeroDeVendedores = 2;

            var vendedorController = new VendedorController(_vendedorRepository);
            var result = await _vendedorRepository.BuscarTodosVendedores();
            
            Assert.Equal(numeroDeVendedores, result.Count());
        }
    }
}