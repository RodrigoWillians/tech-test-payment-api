using apipagamento_api.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using tech_test_payment_api.DatabaseContext;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Repository;
using tech_test_payment_api.Repository.Interfaces;
using tech_test_payment_api_test;

namespace apipagamento_api_test
{
    public class VendasControllerTest
    {
        private static VendaRepository _vendaRepository;

        static VendasControllerTest()
        {
            var dbOptions = new DbContextOptionsBuilder<PaymentContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;
            var context = new PaymentContext(dbOptions);
            var db = new DbUnitTestMockInitializer();
            db.Seed(context);
            _vendaRepository = new VendaRepository(context);
        }

        [Fact]
        public async Task GetVendedorPorId_EDeveRetornarSomenteUmVendedor()
        {
            //Arrange
            var expectedid = 1;
            //Act
            var result = await _vendaRepository.GetVendedorAsync(1);

            //Assert
            Assert.Equal(expectedid, result.Id);
        }

        [Fact]
        public async Task GetProduto_E_DeveRetornarSomenteUmProduto()
        {
            //Arrange
            var expectedid = 1;
            //Act
            var result = await _vendaRepository.GetProdutoAsync(1);

            //Assert
            Assert.Equal(expectedid, result.Id);
        }

        [Fact]
        public async Task GetUltimaVenda_E_DeveRetornarAUltimaVendaCadastrada()
        {
            //Arrange
            var expectedid = 1;
            //Act
            var result = await _vendaRepository.GetVendaLastAsync();

            //Assert
            Assert.Equal(expectedid, result.Id);
        }

        [Fact]
        public async Task GetVendaPeloId_E_DeveRetornarAVenda()
        {
            //Arrange
            var expectedid = 1;
            //Act
            var result = await _vendaRepository.GetVendaIDAsync(1);

            //Assert
            Assert.Equal(expectedid, result.Id);
        }


        Mock<IVendaRepository> _vendaRepositoryMock;
        VendaController _vendaController;

        public VendasControllerTest()
        {
            _vendaRepositoryMock = new Mock<IVendaRepository>();
            _vendaController = new VendaController(_vendaRepositoryMock.Object);

        }

        [Fact]
        public async Task MandaAtualizarPagamentoCancelado_OndeStatusInicialAguardandoPagamento_DeveAlterarStatusVenda()
        {
            var vendaUpdate = new VendaUpdateDTO(1,
                                                 1,
                                                 "Cancelado",
                                                 10.00M);

            var result = await _vendaController
                                        .UpdateStatusVenda(vendaUpdate, 1) as ObjectResult;

            Assert.Equal(400, result.StatusCode);

        }
    }
}