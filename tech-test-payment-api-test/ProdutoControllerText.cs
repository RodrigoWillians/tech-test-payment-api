using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Controllers;
using tech_test_payment_api.DatabaseContext;
using tech_test_payment_api.Model;
using tech_test_payment_api.Repository;
using tech_test_payment_api.Repository.Interfaces;

namespace tech_test_payment_api_test
{
    public class ProdutoControllerTest
    {

        private static IProdutoRepository _produtoRepository;


        static ProdutoControllerTest()
        {
            var dbOptions = new DbContextOptionsBuilder<PaymentContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;
            var context = new PaymentContext(dbOptions);
            var db = new DbUnitTestMockInitializer();
            db.Seed(context);
            _produtoRepository = new ProdutoRepository(context);
        }

        [Fact]
        public async Task GetProdutoId_DeveRetornarSomenteUmProduto() {
            var expectedid = 1;
            var produtoController = new ProdutoController(_produtoRepository);
            var result = await _produtoRepository.BuscarProduto(expectedid, true);
            Assert.Equal(expectedid, result.Id);
        }

        [Fact]
        public async Task GetTodosProduto_DeveRetornarSomenteUmProduto() {
            List<Produto> produto = new List<Produto>();

            var numeroDeProdutos = 2;

            var produtoController = new ProdutoController(_produtoRepository);
            var result = await _produtoRepository.BuscarTodosProdutos();
            
            Assert.Equal(numeroDeProdutos, result.Count());
        }
    }
}